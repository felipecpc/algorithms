package removeduplicates;

import java.util.HashSet;
import java.util.Set;

public class RemovingDuplicates {

	private static int myArray[] = {0,1,2,9,5,7,2,4,9,4,5,0,1,4,7,1,8,5,9,0};
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for(Integer value: hashSet(myArray)){
			System.out.print(value+",");
		}
	}
	
	static Set<Integer> hashSet(int [] array){
		
		Set<Integer> mySet = new HashSet<Integer>();
		for(int x=0;x<array.length;x++){
			mySet.add(array[x]);
		}
		
		return mySet;
	}
	
}
