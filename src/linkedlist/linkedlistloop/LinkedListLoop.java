package linkedlist.linkedlistloop;

public class LinkedListLoop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LinkedList lList = new LinkedList(new Node("1"));
		lList.addToTail(new Node("2"));
		lList.addToTail(new Node("3"));
		lList.addToTail(new Node("4"));
		lList.addToTail(new Node("5"));
		lList.addToTail(new Node("6"));
		lList.addToTail(new Node("7"));
		lList.addToTail(new Node("8"));
		lList.addToTail(new Node("9"));
		lList.addToTail(new Node("10"));
		
		System.out.println("LinkedList loop size: " + lList.isLooped());
		
		lList = new LinkedList(new Node("1"));
		Node n = new Node("2");
		lList.addToTail(n);
		lList.addToTail(new Node("3"));
		lList.addToTail(new Node("4"));
		lList.addToTail(new Node("5"));
		lList.addToTail(new Node("6"));
		lList.addToTail(new Node("7"));
		lList.addToTail(new Node("8"));
		lList.addToTail(new Node("9"));
		lList.addToTail(n);
		
		System.out.println("LinkedList loop size: " + lList.isLooped());
		
	}

}
