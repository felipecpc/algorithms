package linkedlist.linkedlistloop;

public class LinkedList {

	private Node mHead;
	
	public LinkedList (Node node){
		this.mHead = node;
	}
	
	public void addToTail(Node node){
		Node n = mHead;
		while (n.getNextNode()!=null){
			n = n.getNextNode();
		}
		
		n.setNextNode(node);
	}
	
	public int isLooped(){
		Node hare = mHead;
		Node tortoise = mHead;
		
		boolean isCounting=false;
		int count=0;
		
		while(hare.getNextNode()!=null && hare.getNextNode().getNextNode()!=null){
			tortoise = tortoise.getNextNode();
			hare = hare.getNextNode().getNextNode();
			
			if(tortoise==hare){
				if(isCounting==true)return count;
				isCounting = true;
			}
			
			if(isCounting==true){
				count++;
			}
			
		}
		return count;
	}
	
}
