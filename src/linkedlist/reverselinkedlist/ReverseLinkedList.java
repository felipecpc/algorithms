package linkedlist.reverselinkedlist;

public class ReverseLinkedList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Node head = new Node("1");
		LinkedList lList = new LinkedList(head);
		lList.addToTail(new Node("2"));
		lList.addToTail(new Node("3"));
		lList.addToTail(new Node("4"));
		lList.addToTail(new Node("5"));
		lList.addToTail(new Node("6"));
		lList.addToTail(new Node("7"));
		lList.addToTail(new Node("8"));
		lList.addToTail(new Node("9"));
		Node tails= new Node("10");
		lList.addToTail(tails);
	
		System.out.println("My linked list");
		System.out.println(lList.toString());
		System.out.println("Reverse linkedlist recursively");
		lList.reverseRecursive(head);
		System.out.println(lList.toString());
		lList.reverseRecursive(tails);
		System.out.println(lList.toString());		
	
		System.out.println("Reverse linkedlist iteratively");
		lList.reverse();
		System.out.println(lList.toString());
		lList.reverse();
		System.out.println(lList.toString());
	}

}
