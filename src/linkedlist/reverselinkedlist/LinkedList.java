package linkedlist.reverselinkedlist;

public class LinkedList {

	private Node mHead;
	
	public LinkedList (Node node){
		this.mHead = node;
	}
	
	public void addToTail(Node node){
		Node n = mHead;
		while (n.getNextNode()!=null){
			n = n.getNextNode();
		}
		
		n.setNextNode(node);
	}
	
	public int isLooped(){
		Node hare = mHead;
		Node tortoise = mHead;
		
		boolean isCounting=false;
		int count=0;
		
		while(hare.getNextNode()!=null && hare.getNextNode().getNextNode()!=null){
			tortoise = tortoise.getNextNode();
			hare = hare.getNextNode().getNextNode();
			
			if(tortoise==hare){
				if(isCounting==true)return count;
				isCounting = true;
			}
			
			if(isCounting==true){
				count++;
			}
			
		}
		return count;
	}

	public String reverse() {
		// TODO Auto-generated method stub
		Node nextNode;
		Node previousNode=null;
		Node current=mHead;
		
		while(current!=null){
			
			nextNode = current.getNextNode();
			current.setNextNode(previousNode);
			previousNode = current;
			current = nextNode;
		}
		
		mHead=previousNode;
		
		return null;
	}
	
	public Node reverseRecursive(Node node) {

		
		if(node.getNextNode()==null){
			mHead = node;
			return node;
		}
		
		reverseRecursive(node.getNextNode());
		
		Node tmp = node.getNextNode();
		tmp.setNextNode(node);
		node.setNextNode(null);
		
		
		return node;
	
	}
	
	public String toString(){
		Node current = mHead;
		while(current!=null){
			System.out.print(current.getStringData()+ ">");
			current=current.getNextNode();
		}
		
		return null;
		
	}
	
}
