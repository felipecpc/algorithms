package maximumsumsubarray;

public class MaximumsumsubarrayPositivePeaks {

	public static int testArray[] = {-1,-3,4,2,-1,4,6,1,-2,3};
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int maxCurrent=testArray[0],maxGlobal = testArray[0];
		
		for(int x=0;x<testArray.length;x++){
			maxCurrent = max(testArray[x],maxCurrent + testArray[x]);
			if(maxCurrent>maxGlobal)maxGlobal=maxCurrent;
			if(testArray[x]<0)maxCurrent=testArray[x];
		}
		
		System.out.println("Max sum is:"+maxGlobal);
	}
	
	private static int max(int current,int value){
		 if(value>current)return value;
		 else return current;
	}

}
