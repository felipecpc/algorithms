package fibonacci;

public class Fibonacci {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(">> " + FibonacciIterative(20));
		System.out.println(">> " + FibonacciRecursive(20));
	}

	
	static int FibonacciIterative(int n)
	{
	    if (n == 0) return 0;
	    if (n == 1) return 1;
	        
	    int prevPrev = 0;
	    int prev = 1;
	    int result = 0;
	        
	    for (int i = 2; i <= n; i++)
	    {
	        result = prev + prevPrev;
	        prevPrev = prev;
	        prev = result;
	    }
	    return result;
	}
	    
	static int FibonacciRecursive(int n)
	{
	    if (n == 0) return 0;
	    if (n == 1) return 1;
	        
	    return FibonacciRecursive(n - 1) + FibonacciRecursive(n - 2);
	}
	
}
