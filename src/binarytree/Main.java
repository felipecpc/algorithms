package binarytree;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinaryTree bt = new BinaryTree();
		bt.addNode(80, "key 80");
		bt.addNode(30, "key 30");
		bt.addNode(20, "key 20");
		bt.addNode(10, "key 10");
		bt.addNode(90, "key 90");
		bt.addNode(100, "key 100");
		
		bt.inOrderTraverseTree(bt.root);
		System.out.println(bt.getHeight(bt.root));
		bt.levelTraverse(bt.root);
		
	}

}
