package binarytree;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {
	Node root;
	public void addNode(int key, String value){
		Node node = new Node(key,value);
		if(root==null){
			root = node;
		}
		else{
			Node focusNode=root;
			Node parent;
			while(true){
				parent = focusNode;
				if(key<focusNode.key){
					focusNode=focusNode.leftNode;
					if(focusNode==null){
						parent.leftNode=node;
						return;
					}
				}
				else{
					focusNode=focusNode.rightNode;
					if(focusNode==null){
						parent.rightNode=node;
						return;
					}
				}
			}
		}		
	}
	
	public void inOrderTraverseTree(Node focusNode){
		if(focusNode!=null){
			inOrderTraverseTree(focusNode.leftNode);
			System.out.println(focusNode);
			inOrderTraverseTree(focusNode.rightNode);
		}
	}
	
	public void preOrderTraverseTree(Node focusNode){
		if(focusNode!=null){
			System.out.println(focusNode);
			preOrderTraverseTree(focusNode.leftNode);
			preOrderTraverseTree(focusNode.rightNode);
		}
	}
	
	public void postOrderTraverseTree(Node focusNode){
		if(focusNode!=null){
			postOrderTraverseTree(focusNode.leftNode);
			postOrderTraverseTree(focusNode.rightNode);
			System.out.println(focusNode);
		}
	}
	
	public Node findNode(int key){
		Node focusNode = root;
		
		while(focusNode.key!=key){
			
			if(focusNode.key>key){
				focusNode=focusNode.leftNode;
			}else{
				focusNode=focusNode.rightNode;
			}
			
			if(focusNode==null)
				return null;
			
		}
		
		return focusNode;
		
	}
	
	
	public boolean remove(int key){
		Node focusNode = root;
		Node parent =root;
		boolean isLeft = true;
		
		while(focusNode.key!=key){
			parent=focusNode;
			if(focusNode.key>key){
				focusNode=focusNode.leftNode;
				isLeft = true;
			}else{
				focusNode=focusNode.rightNode;
				isLeft=false;
			}
			
			if(focusNode==null)
				return false;
			
		}
		
		if(focusNode.leftNode==null && focusNode.rightNode==null){
			if(focusNode==root){
				root=null;
			}else if (isLeft){
				parent.leftNode = null;
			}else if(!isLeft){
				parent.rightNode = null;
			}
		}else if(focusNode.rightNode==null){
			if(focusNode==root){
				root=focusNode.leftNode;
			}else if (isLeft){
				parent.leftNode = focusNode.leftNode;
			}else if(!isLeft){
				parent.rightNode = focusNode.rightNode;
			}
		}else if(focusNode.leftNode==null){
			if(focusNode==root){
				root=focusNode.rightNode;
			}else if (isLeft){
				parent.leftNode = focusNode.rightNode;
			}else if(!isLeft){
				parent.rightNode = focusNode.leftNode;
			}
		}else{
			Node replacement = replacementNode(focusNode);
			if(focusNode==root){
				root=replacement;
			}else if (isLeft){
				parent.leftNode = replacement;
			}else if(!isLeft){
				parent.rightNode = replacement;
			}
			
			replacement.leftNode = focusNode.leftNode;
		}
		
		
		
		return true;
		
	}

	private Node replacementNode(Node replacedNode) {
		Node replacementParent = replacedNode;
		Node replacement = replacedNode;
		Node focusNode = replacedNode.rightNode;
		
		while(focusNode!=null){
			replacementParent = replacement;
			replacement = focusNode;
			focusNode = focusNode.leftNode;
		}
		// TODO Auto-generated method stub
		
		if(replacement!=replacementParent.rightNode){
			replacementParent.leftNode=replacement.rightNode;
			replacement.rightNode=replacedNode.rightNode;

		}
		return replacement;
	}
	
	public int getHeight (Node focusNode){
		int h1=0;
		int h2=0;
		if(focusNode!=null){
			h1=getHeight(focusNode.leftNode);
			h2=getHeight(focusNode.rightNode);
			
		}else{
			return -1;
		}
		
		if(h1>h2)return (h1+1);
		else return h2+1;
		
		
	}
	
	public void top_view(Node root)
	{
        if(root.leftNode!=null){
            root.leftNode.rightNode=null;
            top_view(root.leftNode);  
        }
        System.out.print(root.name + " ");
        if(root.rightNode!=null){
            root.rightNode.leftNode=null;
            top_view(root.rightNode);    
        }
	}
	
	
	public void levelTraverse(Node root)
	{
		Queue<Node> queue=new LinkedList<Node>();  
		queue.add(root);
		Node currentNode;
		//if queue not empty
		while(!queue.isEmpty()){
			currentNode = queue.poll();
			System.out.print(currentNode.name + " ");
			if(currentNode.leftNode!=null)queue.add(currentNode.leftNode);
			if(currentNode.rightNode!=null)queue.add(currentNode.rightNode);
			
		}
	}
	
	
	
}
