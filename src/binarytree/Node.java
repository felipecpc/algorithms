package binarytree;

public class Node {

	String name;
	int key;
	
	Node leftNode;
	Node rightNode;
	
	Node(int key,String name){
		this.key = key;
		this.name=name;
	}
	
	public String toString(){
		return "Key is: "+ key + " Name is:" + name;
	}
}
