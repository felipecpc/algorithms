package findenegativeonmatrix;

public class NegativeOnMatrix {

	static int testMatrix1 [][] = new int[4][4] ;
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		initTestMatrix();
		System.out.println(countNegative(testMatrix1));
		
	
	}

	private static int countNegative(int matrix[][]){

		int m=0;
		int n=matrix[0].length-1;
		int count=0;
		
		for(int x=0;x<((matrix[0].length)*(matrix[0].length));x++){
			
			if(matrix[m][n]>=0){
				n--;
			}
			else{
				count += n+1;
				m++;
			}
			
			if(m==matrix[0].length || n<0) break;
			
		}
		return count;
	}
	
	private static void initTestMatrix (){
		testMatrix1[0][0] = -1;
		testMatrix1[0][1] = -1;
		testMatrix1[0][2] = -1;
		testMatrix1[0][3] = 0;
		
		testMatrix1[1][0] = -1;
		testMatrix1[1][1] = -1;
		testMatrix1[1][2] = 0;
		testMatrix1[1][3] = 0;
		
		testMatrix1[2][0] = -1;
		testMatrix1[2][1] = 0;
		testMatrix1[2][2] = 0;
		testMatrix1[2][3] = 0;
		
		testMatrix1[3][0] = -1;
		testMatrix1[3][1] = 0;
		testMatrix1[3][2] = 0;
		testMatrix1[3][3] = 0;
		
	}
}
